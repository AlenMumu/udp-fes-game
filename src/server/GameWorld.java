package server;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;

/**
 * Created by amujkic on 23.04.2014.
 */
public class GameWorld extends World {
    public GameWorld(Vec2 gravity, boolean doSleep) {
        super(gravity, doSleep);
    }
}
