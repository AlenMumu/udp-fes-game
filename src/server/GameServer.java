package server;

import packets.Packet;
import packets.Packet00Login;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.util.ArrayList;

/**
 * Created by amujkic on 23.04.2014.
 */
public class GameServer implements Runnable{



    public ArrayList<GameClient> clients = new ArrayList<>();

    private InetAddress ipAddress;
    private DatagramSocket socket;
    private ServerSocket serverSocket;
    private Socket reliableSocket;
    Lobby gameLobby = new Lobby(this);

    public GameServer(String ipAddress) {
    Thread thread = new Thread(this);
        thread.start();
        try {
            this.socket = new DatagramSocket(11331);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        acceptConnections();
    }



    public void acceptConnections(){

        while (true) {
            byte[] data = new byte[1024];
            DatagramPacket packet = new DatagramPacket(data, data.length);
            try {
               // System.out.println("Waiting for connections...");
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.parsePacket(packet.getData(), packet.getAddress(), packet.getPort());


        }

    }
    int i =0;

    public void parseReliablePacket(byte[] data,InetAddress address, int port,InputStream in, OutputStream out){

        String message = new String(data).trim();
        Packet.PacketTypes type = Packet.lookupPacket(message.substring(0, 2));
        Packet packet = null;
        switch (type) {
            default:
            case INVALID:
                break;
            case LOGIN:

                packet = new Packet00Login(data);
                System.out.println("[" + address.getHostAddress() + ":" + port + "] "
                        + ((Packet00Login) packet).getUsername() + " has connected...");


                boolean exists = false;
                for(GameClient client : clients){

                    exists = client.exists(((Packet00Login) packet).getUsername());

                }

                if(!exists) {
                    System.out.println("ADDED CLIENT");

                    GameClient client = new GameClient(((Packet00Login) packet).getUsername(),socket,address,((Packet00Login) packet).getPort(),100,200,in,out);

                    clients.add(client);
                    gameLobby.getClients().add(client);
                    client.setLobby(gameLobby);

                    client.start();
                }



                break;



        }

    }

    public void parsePacket(byte[] data,InetAddress address, int port){

        String message = new String(data).trim();
        Packet.PacketTypes type = Packet.lookupPacket(message.substring(0, 2));
        Packet packet = null;
        switch (type) {
            default:
            case INVALID:
                break;




        }

    }

    InputStream in;
    OutputStream out;


    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(11330);

            while(true){
                reliableSocket = serverSocket.accept();

                out = reliableSocket.getOutputStream();
                in = reliableSocket.getInputStream();

                byte[] bytes = new byte[1024];

                in.read(bytes);


                parseReliablePacket(bytes,reliableSocket.getInetAddress(),reliableSocket.getPort(),in,out);

                try {

                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
