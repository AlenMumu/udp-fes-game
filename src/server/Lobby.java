package server;

import entities.ServerWeapon;
import packets.Packet04Item;
import packets.Packet06Room;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by amujkic on 13.05.2014.
 */
public class Lobby {

    private ArrayList<GameClient> clients = new ArrayList<>();
    private ArrayList<GameSession> gameSessions = new ArrayList<>();
    private GameServer server;

    public Lobby(GameServer server) {
        this.server = server;

    }


    public void createGame(GameClient client,final String roomName,final String mapName,final int maxPlayers){


        GameSession session = new GameSession(roomName,mapName,maxPlayers);
        gameSessions.add(session);
        client.entity.setX((int)session.getSpawnPoints().get(0).x*30);
        client.entity.setY((int)session.getSpawnPoints().get(0).y*30);

        session.getSpawnPoints().remove(0);

        client.setCurrentGame(session);




        for(ServerWeapon w : session.getWeapons()){

            Packet04Item weapon = new Packet04Item(w.getName(),w.getTextureName(),"none",w.getWeaponBody().getPosition().x,w.getWeaponBody().getPosition().y,w.getWidth(),w.getHeight(),w.getId().toString());

            client.sendDataToClientReliable(weapon, client.getIpAddress(), client.port);

        }




        for (GameClient client1 : getClients()) {
            client1.sendDataToClientReliable(new Packet06Room(maxPlayers,session.getClients().size(),roomName,mapName), client1.getIpAddress(), client1.port);

            client.sendDataToClientReliable(new Packet06Room(maxPlayers,session.getClients().size(),roomName,mapName), client.getIpAddress(), client.port);


        }

        if (session.getClients().size() == session.getMaxPlayers()) {
            session.start();
            System.out.println("STARTED"+session.getMaxPlayers()+" "+session.getClients().size());
        }else{
            System.out.println("DIDNT START THE SESSION"+session.getMaxPlayers()+" "+session.getClients().size());
        }

    }

    public void joinGame(GameClient client,final UUID roomId){


        for(GameSession session : gameSessions) {

            if(session.getRoomId().equals(roomId)) {
                client.entity.setX((int)session.getSpawnPoints().get(0).x*30);
                client.entity.setY((int)session.getSpawnPoints().get(0).y*30);

                session.getSpawnPoints().remove(0);

                client.setCurrentGame(session);
                for(ServerWeapon w : session.getWeapons()){

                    Packet04Item weapon = new Packet04Item(w.getName(),w.getTextureName(),"none",w.getWeaponBody().getPosition().x,w.getWeaponBody().getPosition().y,w.getWidth(),w.getHeight(),w.getId().toString());

                    client.sendDataToClientReliable(weapon, client.getIpAddress(), client.port);



                }



                if (session.getClients().size() == session.getMaxPlayers()) {
                    session.start();
                    System.out.println("STARTED"+session.getMaxPlayers()+" "+session.getClients().size());
                }else{
                    System.out.println("DIDNT START THE SESSION"+session.getMaxPlayers()+" "+session.getClients().size());
                }
break;
            }
        }
    }

    public ArrayList<GameClient> getClients() {
        return clients;
    }

    public void setClients(ArrayList<GameClient> clients) {
        this.clients = clients;
    }

    public ArrayList<GameSession> getGameSessions() {
        return gameSessions;
    }

    public void setGameSessions(ArrayList<GameSession> gameSessions) {
        this.gameSessions = gameSessions;
    }
}
