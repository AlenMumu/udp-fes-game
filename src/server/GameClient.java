package server;

import entities.Character;
import entities.ServerWeapon;
import entities.Weapon;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.joints.RevoluteJoint;
import org.jbox2d.dynamics.joints.RevoluteJointDef;
import packets.*;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.UUID;

/**
 * Created by amujkic on 23.04.2014.
 */
public class GameClient extends Thread{

    public volatile Character entity;
    private volatile InetAddress ipAddress;
    private volatile DatagramSocket socket;
    private volatile String userName;
    private volatile GameSession currentGame;
    private volatile Lobby lobby;
// WEAPON BODY
    private volatile int rateOfFire = 1;
    private volatile double angle;
    private volatile BodyDef weaponBodyDef;
    private volatile Body weaponBody;
    private volatile float densityW = 0.1f;
    private volatile float frictionW = 0.7f;
    private volatile String weaponTexture;


    // PLAYER BODY
    private volatile Body body;
    private volatile float density = 5000f;
    private volatile float friction = 0.1f;
    private volatile FixtureDef boxFixture;

    volatile int port;
    volatile DataInputStream inputStream;
    volatile DataOutputStream outputStream;
    public GameClient(String userName,DatagramSocket socket, InetAddress address, int port, int spawnX, int spawnY, InputStream inputStream, OutputStream outputStream){

       this.outputStream = new DataOutputStream(outputStream);
       this.inputStream = new DataInputStream(inputStream);


        this.userName = userName;

        this.socket = socket;
        this.ipAddress = address;

        this.port = port;

        this.entity = new Character(userName,spawnX*30f,spawnY*30f,0);
        this.weaponTexture = this.entity.getWeapon().getTextureName();


    }

    @Override
    public void run() {

        Thread reliableThread = new Thread(new Runnable() {
            @Override
            public void run() {

                while(true){


                    byte[] bytes = new byte[1024];

                    try {
                        bytes = inputStream.readUTF().getBytes();
                        parsePacket(bytes, ipAddress, 11330);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
        reliableThread.start();
        while (true) {

                byte[] data = new byte[1024];
                DatagramPacket packet = new DatagramPacket(data, data.length);
                try {

                    socket.receive(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                this.parsePacket(packet.getData(), packet.getAddress(),packet.getPort());

            }

    }

    private void parsePacket(byte[] data, InetAddress address, int port) {
        String message = new String(data).trim();
        Packet.PacketTypes type = Packet.lookupPacket(message.substring(0, 2));
        Packet packet = null;
        switch (type) {
            default:
            case INVALID:
                break;

            case MOVE:
                packet = new Packet02Move(data);
                if(((Packet02Move)packet).getUsername().equals(userName)){

                    System.out.println(new String(data)+" JUST RECEIVED THIS HERE");

                    float moveDirX = ((Packet02Move)packet).getX();
                    float moveDirY = ((Packet02Move)packet).getY();


                    if(moveDirX<0){


                            body.setLinearVelocity(new Vec2(-10f, body.getLinearVelocity().y));

                    }

                    if(moveDirX>0){

                            body.setLinearVelocity(new Vec2(10f, body.getLinearVelocity().y));

                    }

                    if(moveDirX==0){

                        body.setLinearVelocity(new Vec2(0,body.getLinearVelocity().y));

                    }

                    if(moveDirY>0){
                        body.setLinearVelocity(new Vec2(body.getLinearVelocity().x,10));
                    }
                    entity.setX((int)body.getPosition().mul(30).x);
                    entity.setY((int)body.getPosition().mul(30).y);
                    entity.setDirection(((Packet02Move)packet).getDirection());
                }




                break;
            case ANGLE:
                packet = new Packet03WeaponAngle(data);
                //  System.out.println();
                if(((Packet03WeaponAngle)packet).getUsername().equals(entity.getWeapon().getName())){
                    float deltaX = ((Packet03WeaponAngle)packet).getX();
float deltaY = ((Packet03WeaponAngle)packet).getY();
                    float angleInDegrees = (float)(Math.atan2(deltaY, deltaX) * 180 / 3.14);
                  //  System.out.println("ANGLE: " + angleInDegrees + "DELTA X " + deltaX + " DELTA Y" + deltaY);


                        entity.getWeapon().setAngle(angleInDegrees);

                }

                break;
            case MESSAGE:


                packet = new Packet99ChatMessage(data);
                System.out.println(((Packet99ChatMessage) packet).getMessage());
                for(GameClient client:currentGame.getClients()){
                    for(GameClient c:currentGame.getClients()) {


                        client.sendDataToClientReliable(((Packet99ChatMessage) packet), client.getIpAddress(), 11330);
                    }
                }
                break;

            case ITEMREQUEST:


                packet = new Packet05RequestPickup(data);
ServerWeapon weaponToRemove = null;
                for(ServerWeapon item:currentGame.getWeapons()){

                    if(item.getWeaponBody().getPosition().mul(30).x -2 < ((Packet05RequestPickup) packet).getX() &&  item.getWeaponBody().getPosition().mul(30).x +2 >((Packet05RequestPickup) packet).getX()){



                            Weapon weapon = new Weapon(userName,item.getTextureName(),0,0,0,entity);
                            weapon.setId(item.getId().toString());
                        System.out.println(((Packet05RequestPickup) packet).getItemId());
                            entity.setWeapon(weapon);

                            Packet05RequestPickup i = new Packet05RequestPickup(0,0,entity.getWeapon().getId(),((Packet05RequestPickup) packet).getUserName());

                            for(GameClient client:currentGame.getClients()){
                                for(GameClient c:currentGame.getClients()) {


                                    client.sendDataToClient(i, client.getIpAddress(), client.port);
                                }
                            }

                        weaponToRemove = item;

                            break;

                        }



                }

                if(weaponToRemove!=null){
                   currentGame.getWeapons().remove(weaponToRemove);
                }

                break;

            case ROOM:



                break;

            case CREATEROOM:

                packet = new Packet07CreateRoom(data);

                Packet07CreateRoom p = (Packet07CreateRoom)packet;


                lobby.createGame(this,p.getRoomName(),p.getMapName(),p.getMaxPlayers());
System.out.println("CREATED GAME");




                break;
            case JOINROOM:

                packet = new Packet10JoinRoom(data);

                Packet10JoinRoom packet10JoinRoom = (Packet10JoinRoom)packet;


                UUID gameID  = null;
for(GameSession gameSession :lobby.getGameSessions()){
if(gameSession.getRoomName().equals(packet10JoinRoom.getRoomName())){
    gameID  = gameSession.getRoomId();
    break;
}
}

                if(gameID!=null){
                    lobby.joinGame(this,gameID);
                    System.out.println("JOINED GAME");

                }

                break;
    }
    }
    public void sendDataToClient(Packet packet, InetAddress ip, int port){

        try {
            //System.out.println(new String(packet.getData()));
                socket.send(new DatagramPacket(packet.getData(), packet.getData().length, ip, port));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sendDataToClientReliable(Packet packet, InetAddress ip, int port){

        try {
// TCP
            byte[] bytes = packet.getData();
            outputStream.writeUTF(new String(bytes)+"\n");
            outputStream.flush();
          // UDP:  socket.send(new DatagramPacket(packet.getData(), packet.getData().length, ip, port));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setCurrentGame(GameSession gameSession){
        System.out.println("GAME SET");
        currentGame = gameSession;


        boxFixture = new FixtureDef();
        BodyDef boxDef = new BodyDef();
        boxDef.position.set(entity.getX() / 30f, (entity.getY()) / 30f);
        boxDef.type = BodyType.DYNAMIC;

        PolygonShape boxShape = new PolygonShape();
        boxShape.setAsBox(35f / 30f, 35f  / 30f);
        body = currentGame.getWorld().createBody(boxDef);

        boxFixture.restitution = 0f;
        boxFixture.friction = friction;
        boxFixture.density = density;
        boxFixture.shape = boxShape;
        boxFixture.filter.groupIndex = -5;
        body.createFixture(boxFixture);
        body.setFixedRotation(true);



        BodyDef weaponDef = new BodyDef();
        weaponDef.position.set((entity.getX())/30f, (entity.getY())/30f);
        weaponDef.type = BodyType.DYNAMIC;

        PolygonShape weaponShape = new PolygonShape();
        weaponShape.setAsBox((70f/2)/30f, (35f/2)/30f);

        weaponBody = currentGame.getWorld().createBody(weaponDef);

        FixtureDef weaponFixture = boxFixture;
        weaponFixture.restitution = 0f;
        weaponFixture.friction = frictionW;
        weaponFixture.density = densityW;
        weaponFixture.shape = weaponShape;
        weaponFixture.filter.groupIndex = -5;
        weaponFixture.isSensor = true;


        weaponBody.createFixture(weaponFixture);
        RevoluteJointDef revJoint = new RevoluteJointDef();
        revJoint.bodyA = body;
        revJoint.bodyB = weaponBody;
        revJoint.localAnchorA = new Vec2((this.entity.getX())/30f,(this.entity.getY()-70/2)/30f);
        revJoint.localAnchorB = new Vec2((this.entity.getX())/30f,(this.entity.getY()-70/2)/30f);

        revJoint.referenceAngle = 0;
        revJoint.enableLimit=true;


       RevoluteJoint weaponJoint;
        revJoint.collideConnected = false;
        weaponJoint = (RevoluteJoint)currentGame.getWorld().createJoint(revJoint);

        //assignedPlayer.getPlayerBody().createFixture(assignedPlayer.getBoxFixture());


        currentGame.addClient(this);




    }

    public InetAddress getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(InetAddress ipAddress) {
        this.ipAddress = ipAddress;
    }

   public boolean exists(String userName){
       if(userName.equals(this.userName)){
           return true;
       }

       return false;
   }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }
}
