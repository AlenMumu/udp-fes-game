package server;

import entities.Ladder;
import entities.Platform;
import entities.ServerWeapon;
import org.jbox2d.common.Vec2;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import packets.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created by amujkic on 23.04.2014.
 */
public class GameSession extends Thread{

private boolean running = true;
    private boolean paused = false;
    private GameWorld world;
private List<GameClient> clients = Collections.synchronizedList(new ArrayList<GameClient>());
private int maxPlayers;
    private UUID roomId;
    private String roomName;
   final Object lock = new Object();

    private TiledMap map;
    private String mapName;
    private ArrayList<Ladder> ladders = new ArrayList<>();
    private ArrayList<Platform> platforms = new ArrayList<>();
    private ArrayList<ServerWeapon> weapons = new ArrayList<>();

    private ArrayList<Vec2> spawnPoints = new ArrayList<>();
private int initialMapPos;
    private int fps = 30;
    private int frameCount = 0;
    public GameSession(String roomName,String mapName,int maxPlayers){
this.mapName = mapName;
        this.maxPlayers = maxPlayers;
        this.roomName = roomName;
        roomId = UUID.randomUUID();
        try {
            Display.create();


setupWorld();

            Display.destroy();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }


    }

    public GameWorld getWorld(){
        return world;

    }


    long lastFpsTime = 1;


public void run(){
    for (GameClient client : getClients()) {


        for (GameClient client1 : getClients()) {


            client1.sendDataToClientReliable(new Packet00Login(client.getUserName(), client.entity.getX(), client.entity.getY(), client.port), client1.getIpAddress(), client1.port);


        }
    }

    for (GameClient client1 : getClients()) {


        client1.sendDataToClientReliable(new Packet11StartStopGame("start"),client1.getIpAddress(),client1.port);


    }

    long lastLoopTime = System.nanoTime();
    final int TARGET_FPS = 60;
    final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;

    // keep looping round til the game ends
    while (true)
    {
        // work out how long its been since the last update, this
        // will be used to calculate how far the entities should
        // move this loop
        long now = System.nanoTime();
        long updateLength = now - lastLoopTime;
        lastLoopTime = now;
        double delta = updateLength / ((double)OPTIMAL_TIME);

        // update the frame counter
        lastFpsTime += updateLength;
        fps++;

        // update our FPS counter if a second has passed since
        // we last recorded
        if (lastFpsTime >= 1000000000)
        {
            System.out.println("(FPS: "+fps+")");
            lastFpsTime = 0;
            fps = 0;
        }

        world.step(1 / 60f, 8, 3);

        for(GameClient client:clients){
             for(GameClient c:clients) {
                 Packet02Move confirm = new Packet02Move(c.getUserName(),(int)c.getBody().getPosition().mul(30).x,(int)c.getBody().getPosition().mul(30).y,c.entity.getDirection());
                 Packet03WeaponAngle weaponAngle = new Packet03WeaponAngle(c.getUserName(),0,0,c.entity.getWeapon().getAngle());

                  client.sendDataToClient(confirm, client.getIpAddress(), client.port);
                client.sendDataToClient(weaponAngle, client.getIpAddress(),client.port);

             //    System.out.println(new String(confirm.getData()));


            }

for(ServerWeapon w : weapons){
    Packet04Item weapon = new Packet04Item(w.getName(),w.getTextureName(),"none",w.getWeaponBody().getPosition().x,w.getWeaponBody().getPosition().y,w.getWidth(),w.getHeight(),w.getId().toString());
    client.sendDataToClient(weapon, client.getIpAddress(), client.port);

}

            // System.out.println("SENDING PLAYER POSITION "+client.getUserName()+" "+(int)client.getBody().getPosition().mul(30).y);
        }

        // we want each frame to take 10 milliseconds, to do this
        // we've recorded when we started the frame. We add 10 milliseconds
        // to this and then factor in the current time to give
        // us our final value to wait for
        // remember this is in ms, whereas our lastLoopTime etc. vars are in ns.
        try {
            long time = (lastLoopTime-System.nanoTime() + OPTIMAL_TIME)/1000000;

            if(time<0){
                time*=-1;
            }
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


}


//    private void gameLoop()
//    {
//        //This value would probably be stored elsewhere.
//        final double GAME_HERTZ = 60;
//        //Calculate how many ns each frame should take for our target game hertz.
//        final double TIME_BETWEEN_UPDATES = 1000000000 / GAME_HERTZ;
//        //At the very most we will update the game this many times before a new render.
//        //If you're worried about visual hitches more than perfect timing, set this to 1.
//        final int MAX_UPDATES_BEFORE_RENDER = 1;
//        //We will need the last update time.
//        double lastUpdateTime = System.nanoTime();
//        //Store the last time we rendered.
//        double lastRenderTime = System.nanoTime();
//
//        //If we are able to get as high as this FPS, don't render again.
//        final double TARGET_FPS = 60;
//        final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;
//
//        //Simple way of finding FPS.
//        int lastSecondTime = (int) (lastUpdateTime / 1000000000);
//
//        while (running)
//        {
//            double now = System.nanoTime();
//            int updateCount = 0;
//
//            if (!paused)
//            {
//
//                    //Do as many game updates as we need to, potentially playing catchup.
//                    while (now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER) {
//
//                        world.step(1 / 60f, 8, 3);
//                        lastUpdateTime += TIME_BETWEEN_UPDATES;
//                        updateCount++;
//                    }
//
//                //If for some reason an update takes forever, we don't want to do an insane number of catchups.
//                //If you were doing some sort of game that needed to keep EXACT time, you would get rid of this.
//                if ( now - lastUpdateTime > TIME_BETWEEN_UPDATES)
//                {
//                    lastUpdateTime = now - TIME_BETWEEN_UPDATES;
//                }
//
//                //Render. To do so, we need to calculate interpolation for a smooth render.
//                float interpolation = Math.min(1.0f, (float) ((now - lastUpdateTime) / TIME_BETWEEN_UPDATES) );
//
//
//                    for (GameClient client : clients) {
//                        for (GameClient c : clients) {
//                            Packet02Move confirm = new Packet02Move(c.getUserName(), (int) c.getBody().getPosition().mul(30).x, (int) c.getBody().getPosition().mul(30).y, 0);
//                            Packet03WeaponAngle weaponAngle = new Packet03WeaponAngle(c.getUserName(), 0, 0, c.entity.getWeapon().getAngle());
//                            client.sendDataToClient(confirm, client.getIpAddress(), client.port);
//                            client.sendDataToClient(weaponAngle, client.getIpAddress(), client.port);
//                        }
//                    }
//
//
//           // System.out.println("SENDING PLAYER POSITION "+client.getUserName()+" "+(int)client.getBody().getPosition().mul(30).y);
//
//
//                lastRenderTime = now;
//
//                //Update the frames we got.
//                int thisSecond = (int) (lastUpdateTime / 1000000000);
//                if (thisSecond > lastSecondTime)
//                {
//                    System.out.println("NEW SECOND " + thisSecond + " " + frameCount);
//                    fps = frameCount;
//                    frameCount = 0;
//                    lastSecondTime = thisSecond;
//                }
//
//                //Yield until it has been at least the target time between renders. This saves the CPU from hogging.
//                while ( now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS && now - lastUpdateTime < TIME_BETWEEN_UPDATES)
//                {
//                    Thread.yield();
//
//                    //This stops the app from consuming all your CPU. It makes this slightly less accurate, but is worth it.
//                    //You can remove this line and it will still work (better), your CPU just climbs on certain OSes.
//                    //FYI on some OS's this can cause pretty bad stuttering. Scroll down and have a look at different peoples' solutions to this.
//                    try {Thread.sleep(1);} catch(Exception e) {}
//
//                    now = System.nanoTime();
//                }
//            }
//        }
//    }
//
//    public void run(){
//        gameLoop();
//    }

    public void setupWorld() {


        try {
            map = new TiledMap("res/levels/" + mapName);


        } catch (SlickException e) {
            e.printStackTrace();
        }
        world = new GameWorld(new Vec2(0,-9.8f),false);

        readXML();


        int objectGroupCount = map.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = map.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {
                System.out.println(map.getObjectName(gi, oi));
                if (map.getObjectName(gi, oi).contains("player")) {

spawnPoints.add(new Vec2(((float) map.getObjectX(gi, oi) + 35f / 2) / 30f, (((float) map.getHeight() * (float) map.getTileHeight()) - (float) map.getObjectY(gi, oi) - (70f / 2)) / 30f));

                    System.out.println("ADDED SPAWN POINTS"+spawnPoints.get(0).x+" "+spawnPoints.get(0).y);

                }


                //System.out.println( map.getObjectProperty(gi, oi, "points", "" ) );
            }

        }

        objectGroupCount = map.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = map.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {

                if (map.getObjectName(gi, oi).equals("origin")) {
                    initialMapPos = map.getHeight() * map.getTileHeight();
                }
            }
        }

        objectGroupCount = map.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = map.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {
                if (map.getObjectName(gi, oi).contains("land")) {
                    //  Platform platform = new Platform((float)map.getObjectX(gi,oi)/30f+(((float)map.getObjectWidth(gi,oi)/30f)/2),(((float)map.getHeight()*(float)map.getTileHeight())-((float)map.getObjectY(gi,oi)+map.getObjectHeight(gi,oi)))/30f,(((float)map.getObjectWidth(gi,oi)))/2,(float)map.getObjectHeight(gi,oi)/2,world,this);
                    Platform platform = new Platform(((float) map.getObjectX(gi, oi) / 30f + (((float) map.getObjectWidth(gi, oi) / 30f)) / 2), (((float) map.getHeight() * (float) map.getTileHeight() - (float) map.getObjectY(gi, oi)) / 30f) - ((map.getObjectHeight(gi, oi) / 30f) / 2), (float) map.getObjectWidth(gi, oi) / 2, (float) map.getObjectHeight(gi, oi) / 2, world);

                    System.out.println("MAP HEIGHT" + map.getHeight());
                    platforms.add(platform);
                }

            }
        }

        objectGroupCount = map.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = map.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {
                if (map.getObjectName(gi, oi).contains("ladder")) {
                    Ladder ladder = new Ladder(((float) map.getObjectX(gi, oi) / 30f + (((float) map.getObjectWidth(gi, oi) / 30f)) / 2), (((float) map.getHeight() * (float) map.getTileHeight() - (float) map.getObjectY(gi, oi)) / 30f) - ((map.getObjectHeight(gi, oi) / 30f) / 2), (float) map.getObjectWidth(gi, oi) / 2, (float) map.getObjectHeight(gi, oi) / 2, world);

                    System.out.println("MAP HEIGHT" + map.getHeight());
                    ladders.add(ladder);
                }

            }
        }


        objectGroupCount = map.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = map.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {
                if (map.getObjectName(gi, oi).contains("weapon")) {


                    ServerWeapon serverWeapon = new ServerWeapon(map.getObjectName(gi, oi),map.getObjectType(gi,oi),getWorld(),((float) map.getObjectX(gi, oi) / 30f + (((float) map.getObjectWidth(gi, oi) / 30f)) / 2), (((float) map.getHeight() * (float) map.getTileHeight() - (float) map.getObjectY(gi, oi)) / 30f) - ((map.getObjectHeight(gi, oi) / 30f) / 2),70f,35f);

                    System.out.println("MAP HEIGHT" + map.getHeight());
                    weapons.add(serverWeapon);
                }

            }
        }

    }

    public void readXML() {
        try {

            File fXmlFile = new File("res/levels/" + mapName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("polygon");

            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
                    Element parent = (org.w3c.dom.Element) eElement.getParentNode();

                    System.out.println("POINTS: " + eElement.getAttribute("points"));
                    float xPos = Float.parseFloat(parent.getAttribute("x"));

                    float yPos = Float.parseFloat(parent.getAttribute("y"));
                    yPos = (map.getHeight() * map.getTileHeight()) - yPos / 30f;
                    Platform platform = new Platform(eElement.getAttribute("points"), xPos, yPos, world);
                    platforms.add(platform);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void addClient(GameClient client){


            clients.add(client);



    }

    public List<GameClient> getClients() {
        return clients;
    }

    public void setClients(List<GameClient> clients) {
        this.clients = clients;
    }

    public ArrayList<Vec2> getSpawnPoints() {
        return spawnPoints;
    }

    public ArrayList<ServerWeapon> getWeapons() {
        return weapons;
    }

    public void setWeapons(ArrayList<ServerWeapon> weapons) {
        this.weapons = weapons;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
