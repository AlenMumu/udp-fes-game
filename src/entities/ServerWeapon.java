package entities;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.dynamics.*;

import java.util.UUID;

/**
 * Created by alen on 4/24/14.
 */
public class ServerWeapon {


    private UUID id;
    private BodyDef weaponBodyDef;
    private Body weaponBody;
    private float density = 0.1f;
    private float friction = 0.7f;
    private String name;
    private int maxAmmo = 100;
private FixtureDef fixture;
    private World world;
private String textureName;
    private float x,y,width,height;

    public ServerWeapon(String name,String textureName,World world,float x, float y, float width,float height){
this.textureName = textureName;
        this.world = world;
this.x=x;
        this.y=y;
        this.name = name;
        this.width= width;
        this.height=height;

        fixture = new FixtureDef();
        BodyDef boxDef = new BodyDef();
        boxDef.position.set(x, y);
        boxDef.type = BodyType.DYNAMIC;

        PolygonShape boxShape = new PolygonShape();
        boxShape.setAsBox((width/5)/30f, (height/3)/30f);

        weaponBody = world.createBody(boxDef);

        fixture.restitution = 0f;
        fixture.friction = friction;
        fixture.density = density;
        fixture.shape = boxShape;
        fixture.filter.groupIndex = -5;
        weaponBody.createFixture(fixture);
        weaponBody.setFixedRotation(false);


this.id = UUID.randomUUID();

    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }



    public BodyDef getWeaponBodyDef() {
        return weaponBodyDef;
    }

    public void setWeaponBodyDef(BodyDef weaponBodyDef) {
        this.weaponBodyDef = weaponBodyDef;
    }

    public Body getWeaponBody() {
        return weaponBody;
    }

    public void setWeaponBody(Body weaponBody) {
        this.weaponBody = weaponBody;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public float getFriction() {
        return friction;
    }

    public void setFriction(float friction) {
        this.friction = friction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxAmmo() {
        return maxAmmo;
    }

    public void setMaxAmmo(int maxAmmo) {
        this.maxAmmo = maxAmmo;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public String getTextureName() {
        return textureName;
    }

    public void setTextureName(String textureName) {
        this.textureName = textureName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
