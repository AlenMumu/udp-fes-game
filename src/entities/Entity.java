package entities;

/**
 * Created by amujkic on 23.04.2014.
 */
public class Entity {

    public String id;
    public float x, y, angle, direction;
private String name;
    public Entity(String name,float x, float y, float angle) {
        this.x = x;
        this.y = y;
        this.angle = angle;
        this.name = name;
        this.direction = 1;


    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getAngle() {
        return angle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public float getDirection() {
        return direction;
    }

    public void setDirection(float direction) {
        this.direction = direction;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
