package entities;

import org.newdawn.slick.opengl.Texture;

/**
 * Created by alen on 4/23/14.
 */
public class Weapon extends Entity{


    private String textureName;
    private Character player;
private Texture texture;
    public Weapon(String name,String textureName, float x, float y,float angle, Character player) {
        super(name, x, y, angle);

        this.textureName = textureName;
        this.player = player;


    }

    public Weapon(String name,String textureName, float x, float y,float angle) {
        super(name, x, y, angle);

        this.textureName = textureName;

    }

    public void setTexture(Texture t){
        texture = t;
    }

    public Texture getTexture() {
        return texture;
    }

    public String getTextureName() {
        return textureName;
    }

    public void setTextureName(String textureName) {
        this.textureName = textureName;
    }

    public void setPlayer(Character player) {
        this.player = player;
    }

    public Character getPlayer() {
        return player;
    }

}
