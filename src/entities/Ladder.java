package entities;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;

/**
 * Created by amujkic on 24.04.2014.
 */
public class Ladder {

    private Body platformBody;
    private float x,y,width,height;

    private World world;


    public Ladder(float x, float y, float width, float height, World world){

        this.world = world;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;



        BodyDef groundDef = new BodyDef();
        groundDef.position.set(x, y);
        groundDef.type = BodyType.STATIC;
        PolygonShape groundShape = new PolygonShape();

        groundShape.setAsBox((width/30f), (height/30f));
        FixtureDef groundFixture = new FixtureDef();
        groundFixture.isSensor = true;
        groundFixture.density = 10000f;
        groundFixture.restitution = 0.1f;
        groundFixture.shape = groundShape;
        platformBody = world.createBody(groundDef);

        platformBody.createFixture(groundFixture);


    }

    public Ladder(String points,float x, float y, World world){

        this.world = world;

        this.x = x;
        this.y = y;


        points = points.replaceAll(" ",",").trim();
        String[] pointsArray = points.split(",");
        Vec2[] pointsInFloat = new Vec2[pointsArray.length/2];
        for(int i = 0;i<pointsArray.length/2;i++){

            Vec2 newPoint = new Vec2(this.x/30f+Float.parseFloat(pointsArray[i])/30f,this.y/30f+Float.parseFloat(pointsArray[i+1])/30f);
            pointsInFloat[i] = newPoint;
            System.out.println("POINT CREATED: "+newPoint.x+" "+newPoint.y);

        }

        BodyDef groundDef = new BodyDef();
        groundDef.position.set(this.x/30f, this.y/30f);
        groundDef.type = BodyType.STATIC;
        PolygonShape groundShape = new PolygonShape();


        groundShape.set(pointsInFloat,pointsInFloat.length);
        FixtureDef groundFixture = new FixtureDef();

        groundFixture.density = 10000f;
        groundFixture.restitution = 0.1f;
        groundFixture.shape = groundShape;
        platformBody = world.createBody(groundDef);

        platformBody.createFixture(groundFixture);



    }

    public Body getPlatformBody() {
        return platformBody;
    }

    public void setPlatformBody(Body platformBody) {
        this.platformBody = platformBody;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }


    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

}
