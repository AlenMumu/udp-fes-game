package entities;

/**
 * Created by alen on 4/23/14.
 */
public class Character extends Entity {



 private Weapon weapon;
    public Character(String name, float x, float y, float angle) {
        super(name, x, y, angle);

        weapon = new Weapon(name,"mp5.png",0,0,0,this);

    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }
}
