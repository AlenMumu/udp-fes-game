package client;

import packets.Packet00Login;
import packets.Packet99ChatMessage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.DatagramSocket;
import java.util.Date;

/**
 * Created by amujkic on 23.04.2014.
 */
public class ClientLauncher extends JPanel {


    private JPanel gui = new JPanel(new BorderLayout());
    private JPanel chatContainer = new JPanel(new BorderLayout());
    private JPanel typeAreaContainer = new JPanel(new BorderLayout());
    private JTextArea console = new JTextArea();
    private JTextField typeArea = new JTextField();
    private JButton sendButton = new JButton("Send");
    private JLabel userName = new JLabel("");
   private JScrollPane scrollPane;
private  Screen screen;


private Menu menu;
    public ClientLauncher(Menu menu,DatagramSocket socket,String ip, String user, Packet00Login login) {
this.menu = menu;

          typeArea.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

                if(e.getKeyCode() == KeyEvent.VK_ENTER){

                    sendChatMessage();
                    screen.requestFocus();

                }

                if(e.getKeyCode() == KeyEvent.VK_ESCAPE){

                    typeArea.setText(null);
                    screen.requestFocus();

                }

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        setPreferredSize(new Dimension(1200,800));
        setSize(new Dimension(1200,800));
        add(gui, BorderLayout.SOUTH);
        gui.setVisible(true);
        gui.setSize(getSize());
        chatContainer.setVisible(true);
        chatContainer.setSize(1200, 200);
        userName.setSize(new Dimension(100,15));
        scrollPane = new JScrollPane(console);
        console.setAutoscrolls(true);
        console.setMaximumSize(chatContainer.getSize());
        console.setBackground(Color.BLACK);
        console.setForeground(Color.WHITE);
        typeArea.setAutoscrolls(true);
        typeAreaContainer.add(typeArea,BorderLayout.CENTER);

        typeAreaContainer.add(userName,BorderLayout.WEST);
        chatContainer.add(typeAreaContainer,BorderLayout.NORTH);
        chatContainer.add(scrollPane, BorderLayout.CENTER);
        chatContainer.add(sendButton,BorderLayout.EAST);
        gui.add(chatContainer, BorderLayout.SOUTH);
        screen = new Screen(1200, 700, socket,ip, this,user,login);

        gui.add(screen,BorderLayout.CENTER);
menu.getFrame().setContentPane(this);

    }
public void setupScreen(){


    final Thread screenThread = new Thread(screen);
    screenThread.start();

    sendButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            sendChatMessage();
        }
    });
    screen.doWork();


}

    private void sendChatMessage() {
        if(!typeArea.getText().isEmpty()){
            Date date = new Date();
            System.out.println("SENT"+date.toString()+userName.getText()+typeArea.getText());

            byte[] bytes = new Packet99ChatMessage(date.toString(),userName.getText(),typeArea.getText()).getData();

            try {
                menu.getOut().writeUTF(new String(bytes)+"\n");
                menu.getOut().flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // UDP: screen.sendDataToServer(new Packet99ChatMessage(date.toString(),userName.getText(),typeArea.getText()));
            // TODO: wissen nicht ob die Nachricht ankommt (UDP PROTOKOLL)
            typeArea.setText(null);
            typeArea.setCaretPosition(0);
        }
    }

    public void setUserName(String userNameText){
        userName.setText(userNameText);
    }

    public JTextField getTypeArea() {
        return typeArea;
    }

    public JTextArea getConsole() {
        return console;
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    public JPanel getGui() {
        return gui;
    }

    public void setGui(JPanel gui) {
        this.gui = gui;
    }
}
