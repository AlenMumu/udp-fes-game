package client;

import entities.Character;
import entities.Entity;
import entities.Weapon;
import org.jbox2d.common.Vec2;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.util.glu.GLU;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.tiled.TiledMap;
import org.newdawn.slick.util.ResourceLoader;
import packets.*;

import java.awt.*;
import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by amujkic on 23.04.2014.
 */
public class Screen extends Canvas implements Runnable {
    /**
     * The fonts to draw to the screen
     */
    private volatile TrueTypeFont font;
    private volatile TrueTypeFont font2;
    private volatile TiledMap map;
    private volatile TiledMap characterMap;

    float lookAtX = 0;
    float lookAtY = 0;

    /**
     * Boolean flag on whether AntiAliasing is enabled or not
     */

    private int layerX = 0;
    private int layerY = 0;
    private boolean antiAlias = true;
    //private Character entity;
    private int width, height;
    private InetAddress ipAddress;
    private DatagramSocket socket;
    private ArrayList<Entity> players = new ArrayList<>();
    private ArrayList<Weapon> weapons = new ArrayList<>();
    private ArrayList<Weapon> removedWeapons = new ArrayList<>();
    private ArrayList<Packet04Item> itemPackets = new ArrayList<>();
    private Texture weaponTexture;
    private Texture playerTexture;
    private Texture mouseTexture;
    private ClientLauncher launcher;
    private ChatRoom chatRoom;
    private boolean receivedItems = false;
    String userName;

    private Audio wavEffect;
    private SpriteSheet spriteSheet;

    public Screen(int width, int height, DatagramSocket socket, String ipAddressName, ClientLauncher launcher, String userName, Packet00Login packet) {
        super();
        this.launcher = launcher;

        try {
            this.socket = socket;

            this.ipAddress = InetAddress.getByName(ipAddressName);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.width = width;
        this.height = height;
        this.userName = userName;
        launcher.setUserName(userName);
        // this.entity = new Character(userName, 0, 0, 0);
        this.chatRoom = new ChatRoom(launcher, this);


        setSize(new Dimension(width, height));


        System.out.println("finished creating screen");
    }


    public void setupScreen(int width, int height) {



        Mouse.setGrabbed(true);
        try {
            Display.setParent(launcher.getScreen());
            Display.setDisplayMode(new DisplayMode(width, height));
            Display.create();


            System.out.println(Keyboard.isCreated());
        } catch (LWJGLException e) {
            e.printStackTrace();
        }


        initGL();


        try {
            map = new TiledMap("res/levels/" + "someFeetUnder.tmx");
            characterMap = new TiledMap("res/levels/" + "characterMap.tmx");

        } catch (SlickException e) {
            e.printStackTrace();
        }


        // load a default java font
        java.awt.Font awtFont = new Font("Times New Roman", Font.BOLD, 24);
        font = new TrueTypeFont(awtFont, antiAlias);

        // load font from file
        try {
            InputStream inputStream = ResourceLoader.getResourceAsStream("res/birdsFont.ttf");

            Font awtFont2 = Font.createFont(Font.TRUETYPE_FONT, inputStream);
            awtFont2 = awtFont2.deriveFont(24f); // set font size
            font2 = new TrueTypeFont(awtFont2, antiAlias);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FileInputStream playerStream = new FileInputStream(new File("res/sp-studio.png"));
            playerTexture = TextureLoader.getTexture("PNG", playerStream);
            playerStream.close();

            // Replace PNG with your file extension
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        }


        try {
            FileInputStream weaponStream = new FileInputStream(new File("res/mp5.png"));
            weaponTexture = TextureLoader.getTexture("PNG", weaponStream);

            weaponStream.close();
            // Replace PNG with your file extension
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        }


        try {
            FileInputStream mouseStream = new FileInputStream(new File("res/crosshair.png"));
            mouseTexture = TextureLoader.getTexture("PNG", mouseStream);

            mouseStream.close();
            // Replace PNG with your file extension
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        }


        try {
            wavEffect = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/m4shot.wav"));
        } catch (IOException e) {
            e.printStackTrace();
        }

//        try {
//            spriteSheet = new SpriteSheet("res/images/tiles/spritesheetvolt_run.png",280,385, Color.transparent);
//
//        } catch (SlickException e) {
//            e.printStackTrace();
//        }


    }

    double deltaY;
    double deltaX;
    public float mouseDownTime = 0;
    int direction = 1;

    public void userInput(double delta) {


        if (Keyboard.isKeyDown(Keyboard.KEY_F)) {
            for (Entity entity : players) {


                if (entity.getName().equals(userName)) {

                    sendDataToServer(new Packet05RequestPickup(entity.getX(), entity.getY(), "", entity.getName()));
                    break;
                }
            }
        }


        for (Entity entity : players) {


            if (entity.getName().equals(userName)) {
//            System.out.println(Mouse.getX());
//            System.out.println(lookAtX);
//            System.out.println(entity.getX());
                float angleXCheck = entity.getX();
                if (lookAtX > 0) {
                    angleXCheck = Display.getWidth() / 2;
                } else {
                    angleXCheck = entity.getX();
                }

                if (Mouse.getX() < angleXCheck) {
                    direction = 1;
                    // System.out.println("LESS THAN 0");
                } else {
                    direction = -1;
                }


                float lookAtX = 0;
                float lookAtY = 0;

                if (entity.getX() >= Display.getWidth() / 2) {

                    lookAtX = (Display.getWidth() / 2);

                } else if (entity.getX() <= (map.getWidth() * map.getTileWidth()) - Display.getWidth() / 2) {

                    lookAtX = entity.getX();

                }
                if (entity.getY() >= Display.getHeight() / 2) {

                    lookAtY = (Display.getHeight() / 2);

                } else if (entity.getY() <= (map.getHeight() * map.getTileHeight()) - Display.getHeight() / 2) {

                    lookAtY = entity.getY();


                }


                deltaX = (direction * Mouse.getX()) - (direction * lookAtX);


                if (direction == -1) {
                    deltaY = (-direction * Mouse.getY()) - (-direction * lookAtY);


                } else {
                    deltaY = (direction * Mouse.getY()) - (direction * lookAtY);


                }


                if (Keyboard.isKeyDown(Keyboard.KEY_T)) {
                    launcher.getTypeArea().grabFocus();

                }


                if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {

                    sendDataToServer(new Packet02Move(userName, 0, 1, direction));
                }

                if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
                    System.out.println("PRESSED MOVE RIGHT");
//                    if(layerX<spriteSheet.getHorizontalCount()-1){
//
//                        layerX+=delta;
//                    }else{
//
//                        layerX=0;
//
//                            if(layerY<spriteSheet.getVerticalCount()-1){
//                                layerY+=delta;
//                            }else{
//                                layerY=0;
//                            }
//                    }
                    sendDataToServer(new Packet02Move(userName, 1, 0, direction));
                } else if (Keyboard.isKeyDown(Keyboard.KEY_A)) {

//                    if(layerX>0){
//                        layerX-=delta;
//
//                    }else{
//                        layerX=0;
//
//                            if(layerY>0){
//                                layerY-=delta;
//                            }else{
//                                layerY=spriteSheet.getVerticalCount()-1;
//                            }
//
//                    }
                    sendDataToServer(new Packet02Move(userName, -1, 0, direction));
                } else {
                    sendDataToServer(new Packet02Move(userName, 0, 0, direction));


                }
                break;
            }
        }


        if (Mouse.isButtonDown(0)) {


            if (mouseDownTime >= delta * 5) {
                wavEffect.playAsSoundEffect(1f, 1f, false);

//            System.out.println("SHOT");
//            System.out.println("DOWN TIME"+mouseDownTime);
//            System.out.println(delta);
                mouseDownTime = 0;


            } else {
                mouseDownTime += delta;
//            System.out.println("DOWN TIME"+mouseDownTime);
//            System.out.println(delta);
            }
            Random r = new Random();

            if (r.nextInt(1) == 0) {


                sendDataToServer(new Packet03WeaponAngle(userName, (float) deltaX, (float) deltaY + 30 + r.nextInt(50), 0));


            } else {


                sendDataToServer(new Packet03WeaponAngle(userName, (float) deltaX, (float) ((deltaY + 30 + r.nextInt(50)) * -1), 0));

            }


        } else {

            sendDataToServer(new Packet03WeaponAngle(userName, (float) deltaX, (float) deltaY, 0));


        }


    }


    public void sendDataToServer(Packet packet) {

        try {
            socket.send(new DatagramPacket(packet.getData(), packet.getData().length, ipAddress, 11331));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void update(double delta) {

        userInput(delta);
        Display.update();
        Display.sync(60);
    }

    private void initGL() {
        glEnable(GL_TEXTURE_2D);
        glClearColor(0.5f, 0.5f, 1, 1);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, Display.getWidth(), 0, Display.getHeight(), -1, 1);
    }

    public static void clear() {
        glClear(GL_COLOR_BUFFER_BIT);
    }


    public void render() {
        clear();


        for (Weapon weaponToRemove : removedWeapons) {

            // glDeleteTextures(weaponToRemove.getTexture().getTextureID());
        }
        removedWeapons.clear();
        glEnable(GL_TEXTURE_2D);

        glPushMatrix();
        glRotated(180, 1, 0, 0);
        glTranslatef(0, -(float) (map.getHeight() * map.getTileHeight()), 0f);
        // glRotated(180, 0, 1, 0);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        //  int  y = initialMapPos+(int)entity.getY()-Display.getHeight()/5;

        //x = (int) player.getPlayerBody().getPosition().mul(30).x;

        for (int i = 0; i < map.getLayerCount(); i++) {
            map.render(0, 0, i);
        }


        glEnd();
        glDisable(GL_BLEND);


        glPopMatrix();

        for (Entity entity : players) {


            if (entity instanceof Character) {

                if (((Character) entity).getWeapon().getTexture() == null) {
                    try {
                        FileInputStream weaponStream = new FileInputStream(new File("res/" + ((Character) entity).getWeapon().getTextureName()));
                        weaponTexture = TextureLoader.getTexture("PNG", weaponStream);
                        ((Character) entity).getWeapon().setTexture(weaponTexture);
                        weaponStream.close();
                        // Replace PNG with your file extension
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Display.destroy();
                        System.exit(1);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Display.destroy();
                        System.exit(1);
                    }
                }

                if (((Character) entity).getName().equals(userName)) {


                    if (((Character) entity).getX() >= Display.getWidth() / 2 && ((Character) entity).getX() <= (map.getWidth() * map.getTileWidth()) - Display.getWidth() / 2) {

                        lookAtX = entity.getX() - Display.getWidth() / 2;

                    } else if (((Character) entity).getX() <= (map.getWidth() * map.getTileWidth()) - Display.getWidth() / 2) {

                        lookAtX = 0;

                    } else {
                        lookAtX = (map.getWidth() * map.getTileWidth()) - Display.getWidth();

                    }


                    if (((Character) entity).getY() >= Display.getHeight() / 2 && ((Character) entity).getY() <= (map.getHeight() * map.getTileHeight()) - Display.getHeight() / 2) {


                        lookAtY = ((Character) entity).getY() - Display.getHeight() / 2;

                    } else if (((Character) entity).getY() <= (map.getHeight() * map.getTileHeight()) - Display.getHeight() / 2) {

                        lookAtY = 0;

                    } else {
                        lookAtY = (map.getHeight() * map.getTileHeight()) - Display.getHeight();

                    }
                    glMatrixMode(GL_MODELVIEW);
                    glLoadIdentity();

                    GLU.gluLookAt(lookAtX, lookAtY, 1,
                            lookAtX, lookAtY, 0,
                            0, 1, 0);
                }


                glColor3f(10.5f, 2.5f, 1.0f);


                glPushMatrix();


                glTexCoord2f(entity.getX(), entity.getY());
                glBindTexture(GL_TEXTURE_2D, playerTexture.getTextureID());


                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                glBegin(GL_TRIANGLES);
                // first triangle, bottom left half
                glTexCoord2f(0, 0);
                glVertex2f(entity.getX() - 35f, entity.getY() - 70f);
                glTexCoord2f(-entity.getDirection(), 0);
                glVertex2f(entity.getX() + 35f, entity.getY() - 70f);
                glTexCoord2f(0, -1);
                glVertex2f(entity.getX() - 35f, entity.getY() + 70f / 2);

                // second triangle, top right half
                glTexCoord2f(-entity.getDirection(), 0);
                glVertex2f(entity.getX() + 35f, entity.getY() - 70f);
                glTexCoord2f(0, -1);
                glVertex2f(entity.getX() - 35f, entity.getY() + 70f / 2);
                glTexCoord2f(-entity.getDirection(), -1);
                glVertex2f(entity.getX() + 35f, entity.getY() + 70f / 2);

                glEnd();
//                glEnable(GL_BLEND);
//           glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//
//               org.newdawn.slick.Image sprite =  spriteSheet.getSprite(layerX, layerY);
//                int w = sprite.getWidth();
//                int h = sprite.getHeight();
//                glRotated(180, 1, 0, 0);
//               glTranslatef(0, h/2, 0f);
//
//                sprite.draw(entity.getX()-w/2,-entity.getY()-h/2,0.5f);
//                glTranslatef(0,-h/2, 0f);

                glPopMatrix();
//glPopMatrix();
//                glPushMatrix();
//                glRotated(180, 1, 0, 0);
//               glTranslatef(0, -(float) (characterMap.getHeight() * characterMap.getTileHeight()), 0f);
//
//
//                // glRotated(180, 0, 1, 0);
//                glEnable(GL_BLEND);
//                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//
//                //  int  y = initialMapPos+(int)entity.getY()-Display.getHeight()/5;
//
//                //x = (int) player.getPlayerBody().getPosition().mul(30).x;
//
//                characterMap.render((int)entity.getX(),-(int)entity.getY(),layer);
//
//                glTranslatef(0, (float) (characterMap.getHeight() * characterMap.getTileHeight()), 0f);
//
//                glEnd();
//                glDisable(GL_BLEND);

                glPushMatrix();

                Vec2 bodyPosition = new Vec2(entity.getX(), entity.getY());


                if (entity.getDirection() > 0) {
                    glTranslatef(bodyPosition.x + 30f, bodyPosition.y + 30f, 0);

                    glRotated(((Character) entity).getWeapon().getAngle(), 0, 0, 1);
                    //  System.out.println("1 "+((Character) entity).getWeapon().getAngle());
                    glTranslatef(-bodyPosition.x - 30f, -bodyPosition.y - 30f, 0);

                } else {

                    glTranslatef(bodyPosition.x + 30f, bodyPosition.y + 30f, 0);

                    glRotated(-((Character) entity).getWeapon().getAngle(), 0, 0, 1);
                    //   System.out.println("2 "+((Character) entity).getWeapon().getAngle());
                    glTranslatef(-bodyPosition.x - 30f, -bodyPosition.y - 30f, 0);
                }
//
//                if (entity.getDirection()>0) {
//                    glTranslatef(bodyPosition.x+ ((Character) entity).getWeapon().getTexture().getTextureWidth()/2, bodyPosition.y, 0);
//
//                    glRotated(180, 0, 1, 0);
//
//                    glTranslatef(-bodyPosition.x, 0, 0);
//
//                }

                glTexCoord2f(bodyPosition.x, bodyPosition.y);
                glBindTexture(GL_TEXTURE_2D, ((Character) entity).getWeapon().getTexture().getTextureID());
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                glBegin(GL_TRIANGLES);

                glTexCoord2f(0, 1);
                glVertex2f(bodyPosition.x, bodyPosition.y + ((Character) entity).getWeapon().getTexture().getTextureHeight() / 2);
                glTexCoord2f(0, 0);
                glVertex2f(bodyPosition.x, bodyPosition.y);
                glTexCoord2f(1, 0);
                glVertex2f(bodyPosition.x + ((Character) entity).getWeapon().getTexture().getTextureWidth() / 2, bodyPosition.y);

                // second triangle, top right half
                glTexCoord2f(1, 0);
                glVertex2f(bodyPosition.x + ((Character) entity).getWeapon().getTexture().getTextureWidth() / 2, bodyPosition.y);
                glTexCoord2f(1, 1);
                glVertex2f(bodyPosition.x + ((Character) entity).getWeapon().getTexture().getTextureWidth() / 2, bodyPosition.y + ((Character) entity).getWeapon().getTexture().getTextureHeight() / 2);
                glTexCoord2f(0, 1);
                glVertex2f(bodyPosition.x, bodyPosition.y + ((Character) entity).getWeapon().getTexture().getTextureHeight() / 2);


                glEnd();


                glDisable(GL_BLEND);


                glPopMatrix();
                glPushMatrix();
                glEnable(GL_TEXTURE_2D);
                glShadeModel(GL_SMOOTH);
                glDisable(GL_DEPTH_TEST);
                glDisable(GL_LIGHTING);

                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                glTranslatef(0, -(float) (map.getHeight() * map.getTileHeight()), 0f);
                glRotated(180, 1, 0, 0);
                font2.drawString(((Character) entity).getX() - 35f, ((Character) entity).getY() + 100, ((Character) entity).getName());
                glRotated(180, 1, 0, 0);

                glTranslatef(0, (float) (map.getHeight() * map.getTileHeight()), 0f);

                glDisable(GL_BLEND);
                glPopMatrix();
                // System.out.println("DRAWING " + ((Character) entity).getName());
            }

        }


        glPushMatrix();


        glTexCoord2f(lookAtX + Mouse.getX(), lookAtY + Mouse.getY());
        glBindTexture(GL_TEXTURE_2D, mouseTexture.getTextureID());

        float mouseX = lookAtX + Mouse.getX();
        float mouseY = lookAtY + Mouse.getY();

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glBegin(GL_TRIANGLES);
        // first triangle, bottom left half
        glTexCoord2f(0, 0);
        glVertex2f(mouseX - 15f, mouseY - 15f);
        glTexCoord2f(-1, 0);
        glVertex2f(mouseX + 15f, mouseY - 15f);
        glTexCoord2f(0, -1);
        glVertex2f(mouseX - 15f, mouseY + 15f / 2);

        // second triangle, top right half
        glTexCoord2f(-1, 0);
        glVertex2f(mouseX + 15f, mouseY - 15f);
        glTexCoord2f(0, -1);
        glVertex2f(mouseX - 15f, mouseY + 15f / 2);
        glTexCoord2f(-1, -1);
        glVertex2f(mouseX + 15f, mouseY + 15f / 2);


        glEnd();
        glPopMatrix();

        synchronized (lock) {
            for (Weapon w : weapons) {

                if (w.getTexture() == null) {
                    try {
                        Texture t = TextureLoader.getTexture("PNG", new FileInputStream(new File("res/" + w.getTextureName() + ".png")));
                        w.setTexture(t);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                glPushMatrix();


                glTexCoord2f(w.getX(), w.getY());
                glBindTexture(GL_TEXTURE_2D, w.getTexture().getTextureID());


                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                glBegin(GL_TRIANGLES);
                // first triangle, bottom left half
//            glTexCoord2f(0, 0);
//            glVertex2f(w.getX() - w.getTexture().getTextureWidth()/5, w.getY());
//            glTexCoord2f(-1, 0);
//            glVertex2f(w.getX() + w.getTexture().getTextureWidth()/5, w.getY());
//            glTexCoord2f(0, -1);
//            glVertex2f(w.getX() - w.getTexture().getTextureWidth()/5,w.getY() + w.getTexture().getTextureHeight()/3);
//
//            // second triangle, top right half
//            glTexCoord2f(-1, 0);
//            glVertex2f(w.getX() + w.getTexture().getTextureWidth()/5, w.getY());
//            glTexCoord2f(0, -1);
//            glVertex2f(w.getX() - w.getTexture().getTextureWidth()/5, w.getY() + w.getTexture().getTextureHeight()/3);
//            glTexCoord2f(-1, -1);
//            glVertex2f(w.getX() + w.getTexture().getTextureWidth()/5, w.getY() + w.getTexture().getTextureHeight()/3);
                glTexCoord2f(0, 1);
                glVertex2f(w.getX(), w.getY() + w.getTexture().getTextureHeight() / 2);
                glTexCoord2f(0, 0);
                glVertex2f(w.getX(), w.getY());
                glTexCoord2f(1, 0);
                glVertex2f(w.getX() + w.getTexture().getTextureWidth() / 2, w.getY());

                // second triangle, top right half
                glTexCoord2f(1, 0);
                glVertex2f(w.getX() + w.getTexture().getTextureWidth() / 2, w.getY());
                glTexCoord2f(1, 1);
                glVertex2f(w.getX() + w.getTexture().getTextureWidth() / 2, w.getY() + w.getTexture().getTextureHeight() / 2);
                glTexCoord2f(0, 1);
                glVertex2f(w.getX(), w.getY() + w.getTexture().getTextureHeight() / 2);

                glEnd();
                glPopMatrix();
            }

        }

    }

    public void doWork() {


        setupScreen(width, height);

        loop();
    }

    public void run() {
        while (true) {
            byte[] data = new byte[1024];
            DatagramPacket packet = new DatagramPacket(data, data.length);
            try {

                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.parsePacket(packet.getData(), packet.getAddress(), packet.getPort());
        }
    }

    private Object lock = new Object();

    public void parsePacket(byte[] data, InetAddress address, int port) {
        String message = new String(data).trim();
        Packet.PacketTypes type = Packet.lookupPacket(message.substring(0, 2));
        Packet packet = null;

        //   System.out.println(new String(data));
        switch (type) {
            default:
            case INVALID:
                break;

            case LOGIN:
                packet = new Packet00Login(data);
                System.out.println("[" + address.getHostAddress() + ":" + port + "] " + ((Packet00Login) packet).getUsername() + " has connected...");


                boolean exists = false;
                for (Entity client : players) {
                    if (client instanceof Character) {
                        exists = ((Character) client).getName().equals(((Packet00Login) packet).getUsername());
                    }


                }
                int i = 0;
                if (!exists) {
                    System.out.println("ADDED CLIENT");
                    Character client = new Character(((Packet00Login) packet).getUsername(), ((Packet00Login) packet).getX(), ((Packet00Login) packet).getY(), 0);

                    System.out.println("CREATED CLIENT CHARACTER");
                    players.add(client);

                }
                break;
            case MOVE:
                packet = new Packet02Move(data);


                 for (Entity entity : players) {


                    if (((Packet02Move) packet).getUsername().equals((entity).getName())) {
                        (entity).setX(((Packet02Move) packet).getX());
                        (entity).setY(((Packet02Move) packet).getY());
                        (entity).setDirection(((Packet02Move) packet).getDirection());
                        //System.out.println(entity.getX() + " " + entity.getY());

                        break;
                    }

                }


                break;

            case ANGLE:
                packet = new Packet03WeaponAngle(data);
                // System.out.println(((Packet02Move)packet).getY());

                for (Entity entity : players) {

                    if (((Packet03WeaponAngle) packet).getUsername().equals((entity).getName())) {
                        ((Character) entity).getWeapon().setAngle(((Packet03WeaponAngle) packet).getAngle());

                        break;
                    }

                }


                break;
            case MESSAGE:

                packet = new Packet99ChatMessage(data);
                chatRoom.addMessage((Packet99ChatMessage) packet);

                break;
            case ITEM:

                packet = new Packet04Item(data);

                boolean contains = false;


                Weapon weapon = new Weapon(((Packet04Item) packet).getName(), ((Packet04Item) packet).getTextureName(), ((Packet04Item) packet).getX() * 30f, ((Packet04Item) packet).getY() * 30f, 0);
                weapon.setId(((Packet04Item) packet).getItemId());
                for (Weapon w : weapons) {

                    if (w.getId().equals(weapon.getId())) {
                        contains = true;
                        w.setX(((Packet04Item) packet).getX() * 30f);
                        w.setY(((Packet04Item) packet).getY() * 30f);
                        break;
                    }

                }

                if (!contains) {

                    weapons.add(weapon);

                }


                break;
            case ITEMREQUEST:

                packet = new Packet05RequestPickup(data);

                System.out.println("WAS OVER A WEAPON");


                Weapon weaponToRemove = null;
                synchronized (lock) {
                    for (Weapon w : weapons) {

                        if (w.getId().equals(((Packet05RequestPickup) packet).getItemId())) {

                            System.out.println(((Packet05RequestPickup) packet).getItemId());
                            for (Entity player : players) {
                                if (((Packet05RequestPickup) packet).getUserName().equals(player.getName())) {
                                    ((Character) player).setWeapon(w);
                                    weaponToRemove = w;
                                    System.out.println(w.getTextureName());


                                    weaponTexture = w.getTexture();


                                    break;

                                }
                            }


                            break;
                        }
                    }
                }


                if (weaponToRemove != null) {
                    synchronized (lock) {
                        removedWeapons.add(weaponToRemove);
                        weapons.remove(weaponToRemove);
                    }
                }


                break;

        }
    }

    long lastFpsTime = 1;
    private int fps = 30;

    public void loop() {

        long lastLoopTime = System.nanoTime();
        final int TARGET_FPS = 60;
        final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;

        // keep looping round til the game ends
        while (true) {
            // work out how long its been since the last update, this
            // will be used to calculate how far the entities should
            // move this loop
            long now = System.nanoTime();
            long updateLength = now - lastLoopTime;
            lastLoopTime = now;
            double delta = updateLength / ((double) OPTIMAL_TIME);

            // update the frame counter
            lastFpsTime += updateLength;
            fps++;

            // update our FPS counter if a second has passed since
            // we last recorded
            if (lastFpsTime >= 1000000000) {
                System.out.println("(FPS: " + fps + ")");
                lastFpsTime = 0;
                fps = 0;
            }

            render();
            update(delta);


            // System.out.println("SENDING PLAYER POSITION "+client.getUserName()+" "+(int)client.getBody().getPosition().mul(30).y);


            // we want each frame to take 10 milliseconds, to do this
            // we've recorded when we started the frame. We add 10 milliseconds
            // to this and then factor in the current time to give
            // us our final value to wait for
            // remember this is in ms, whereas our lastLoopTime etc. vars are in ns.
            try {
                long time = (lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000;

                if (time < 0) {
                    time *= -1;
                }
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    public ArrayList<Entity> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Entity> players) {
        this.players = players;
    }

    public ChatRoom getChatRoom() {
        return chatRoom;
    }

    public void setChatRoom(ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
    }

    public DatagramSocket getSocket() {
        return socket;
    }

    public void setSocket(DatagramSocket socket) {
        this.socket = socket;
    }

}
