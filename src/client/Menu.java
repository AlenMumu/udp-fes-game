package client;

import packets.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by amujkic on 13.05.2014.
 */
public class Menu{


    private ArrayList<String> availableGames = new ArrayList<>();
    private DataOutputStream out;
    private DataInputStream in;
    private JPanel panel1;
    private JButton loginButton;
    private JButton startServerButton;
    private JTextField usernameField;
    private JTextField serverIpField;
    private JSpinner spinner1;
    private JLabel myIp;
    private JPanel loginForm;
    private JPanel lobbyForm;
    private JButton backToMenuButton;
    private JTextField roomNameField;
    private JSpinner maxPlayersSpinner;
    private JList list1;
    private JButton joinRoomButton;
    private JComboBox mapsComboBox;
    private JButton createRoomButton;
    private ClientLauncher launcher;
    private InetAddress ipAddress;
    private DatagramSocket socket;
    private Socket reliableSocket;
    private ArrayList<Packet00Login> loginPackets = new ArrayList<>();
    JFrame frame;
private ArrayList<Packet06Room> rooms = new ArrayList<>();
    private Packet00Login loginPacket;
    public Menu() {


        try {
            myIp.setText(Inet4Address.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        frame = new JFrame("Menu");
        frame.setVisible(true);
        frame.setContentPane(this.panel1);
        frame.setSize(new Dimension(1200, 800));
        frame.setTitle("Game");
        frame.setResizable(false);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String userName = usernameField.getText();
                String remoteIp = serverIpField.getText();
                String remotePort = String.valueOf(spinner1.getValue());
                // TODO: SEND LOGIN PACKET TO SERVER

                try {

                    ipAddress = InetAddress.getByName(remoteIp);
                  socket = new DatagramSocket();
                }  catch (UnknownHostException e2) {
                    e2.printStackTrace();
                } catch (SocketException e1) {
                    e1.printStackTrace();
                }

                createReliableSocketConnection();


                lobbyForm.setVisible(true);
                loginForm.setVisible(false);

//                ClientLauncher launcher = new ClientLauncher();
//                frame.setLayout(new BorderLayout());
//                frame.getContentPane().add(launcher,BorderLayout.CENTER);
//                launcher.setupScreen(socket,remoteIp,userName);


            }
        });


        joinRoomButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                for (String game : availableGames) {

                    if (game.equals(String.valueOf(list1.getSelectedValue()))) {

                        // SEND JOIN ROOM COMMAND TO SERVER
                        String roomName = String.valueOf(list1.getSelectedValue());

                        if(!roomName.isEmpty()){

                            Packet06Room roomPacket = null;

                            for(Packet06Room room : rooms){
                                if(room.getRoomName().equals(roomName)){

                                    roomPacket = room;
                                    break;
                                }
                            }
                            if(roomPacket!=null){
                                Packet10JoinRoom joinRoom = new Packet10JoinRoom(roomPacket.getMaxPlayers(),roomPacket.getRoomName(),roomPacket.getMapName());

                                // SÈND CREATE ROOM COMMAND VIA TCP
                                byte[] bytes = joinRoom.getData();

                                try {
                                    out.writeUTF(new String(bytes)+"\n");
                                    System.out.println(new String(bytes));
                                    out.flush();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }



                            }

                        }

                        break;
                    }

                }

            }

        });
        createRoomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                String roomName = roomNameField.getText();
                int maxPlayers = (int) maxPlayersSpinner.getValue();
                String mapName = mapsComboBox.getSelectedItem().toString();

                // SÈND CREATE ROOM COMMAND VIA TCP
                byte[] bytes = new Packet07CreateRoom(maxPlayers, roomName, mapName).getData();

                try {
                    out.writeUTF(new String(bytes)+"\n");
                    System.out.println(new String(bytes));
                    out.flush();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }



                }
        });
        startServerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                System.out.println("Started Server...");

            }
        });
        backToMenuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lobbyForm.setVisible(false);
                loginForm.setVisible(true);
            }
        });
    }


    public static void main(String[] args) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            // If Nimbus is not available, fall back to cross-platform
            try {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception ex) {
                // not worth my time
            }
        }
        Menu menu = new Menu();

    }


    public void sendDataToServer(Packet packet) {

        try {
            socket.send(new DatagramPacket(packet.getData(), packet.getData().length, ipAddress, 11331));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void createReliableSocketConnection() {




        Thread reliableConnectionThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    reliableSocket = new Socket(ipAddress, 11330);
                    out = new DataOutputStream(reliableSocket.getOutputStream());
                    in = new DataInputStream(reliableSocket.getInputStream());

                    Scanner scanner = new Scanner(in);

                    loginPacket = new Packet00Login(usernameField.getText(), 0, 0,socket.getLocalPort());



                    // UDP: sendDataToServer(loginPacket);
System.out.println(socket.getPort());
                    System.out.println(socket.getLocalPort());
                    // TCP
                    try {
                        out = new DataOutputStream(reliableSocket.getOutputStream());
                        out.writeUTF(new String(loginPacket.getData())+"\n");

                        out.flush();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                    while (true) {
                        String line = scanner.nextLine();
                        System.out.println("READING RELIABLE INPUT STREAM");
                        System.out.println(line);
                        parsePacket(line.getBytes(), ipAddress, 11330);
                        try {

                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        reliableConnectionThread.start();




        System.out.println("STARTED UDP THREAD");
    }

    private void parsePacket(byte[] data, InetAddress address, int port) {
        String message = new String(data).trim();
        Packet.PacketTypes type = Packet.lookupPacket(message.substring(0, 2));
        Packet packet = null;

        //System.out.println(new String(data));

        switch (type) {
            default:
            case INVALID:
                break;

            case LOGIN:
                packet = new Packet00Login(data);
                System.out.println("[" + address.getHostAddress() + ":" + port + "] " + ((Packet00Login) packet).getUsername() + " has connected...");


                if(!loginPackets.contains((Packet00Login)packet)){
                    loginPackets.add((Packet00Login)packet);
                }



                break;

            case MESSAGE:

                packet = new Packet99ChatMessage(data);
                launcher.getScreen().getChatRoom().addMessage((Packet99ChatMessage) packet);

                break;
            case STARTSTOP:

                packet = new Packet11StartStopGame(data);

                if(((Packet11StartStopGame)packet).getStartStop().equals("start")) {

                        // UDP: sendDataToServer(new Packet07CreateRoom(maxPlayers,roomName,mapName));
                        launcher = new ClientLauncher(Menu.this, socket, serverIpField.getText(), usernameField.getText(), loginPacket);
                        frame.setLayout(new BorderLayout());
                        frame.setSize(launcher.getSize());
                        frame.setContentPane(launcher);

                        // launcher.setupScreen();

                    for(Packet00Login p :loginPackets) {
                        launcher.getScreen().parsePacket(p.getData(), address, ((Packet00Login) p).getPort());
                    }

                    launcher.setupScreen();
                }
                break;
            case ROOM:

                packet = new Packet06Room(data);
DefaultListModel model = new DefaultListModel();

                for(int i = 0; i<=list1.getModel().getSize()-1;i++){

                    model.addElement(list1.getModel().getElementAt(i));


                }

                model.addElement(((Packet06Room) packet).getRoomName());
rooms.add(((Packet06Room) packet));
                availableGames.add(((Packet06Room) packet).getRoomName());
list1.setModel(model);

                break;

        }
    }

    public DataOutputStream getOut() {
        return out;
    }

    public void setOut(DataOutputStream out) {
        this.out = out;
    }

    public DataInputStream getIn() {
        return in;
    }

    public void setIn(DataInputStream in) {
        this.in = in;
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }


    public JList getList1() {
        return list1;
    }

    public void setList1(JList list1) {
        this.list1 = list1;
    }
}
