package client;

import packets.Packet99ChatMessage;

import java.util.ArrayList;

/**
 * Created by amujkic on 25.04.2014.
 */
public class ChatRoom {
    private ArrayList<Packet99ChatMessage> messages = new ArrayList<>();

private ClientLauncher mainGui;
    private Screen screen;
    public ChatRoom(ClientLauncher mainGui, Screen screen){
this.screen = screen;
        this.mainGui = mainGui;
    }

    public void addMessage(Packet99ChatMessage message){

        messages.add(message);
        updateChat();
    }

    public void updateChat(){
        mainGui.getConsole().setText("");

        for(Packet99ChatMessage packet:messages){

            mainGui.getConsole().append("\n["+packet.getDate()+" "+packet.getUsername()+"] "+packet.getMessage());
            mainGui.getConsole().setCaretPosition(mainGui.getConsole().getDocument().getLength());
        }

    }
}
