package packets;

/**
 * Created by amujkic on 23.05.2014.
 */
public class Packet10JoinRoom extends Packet{



    private int maxPlayers;
    private String roomName;
    private String mapName;



    public Packet10JoinRoom(byte[] data) {
        super(10);
        String[] dataArray = readData(data).split(",");
        this.maxPlayers = Integer.valueOf(dataArray[0]);
        this.roomName = dataArray[1];
        this.mapName = dataArray[2];
    }

    public Packet10JoinRoom(int maxPlayers, String roomName, String mapName) {
        super(10);

        this.maxPlayers = maxPlayers;

        this.roomName = roomName;
        this.mapName = mapName;
    }

    @Override
    public byte[] getData() {
        return  new String("10"+this.maxPlayers+","+this.roomName+","+this.mapName).getBytes();
    }


    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }



    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

}
