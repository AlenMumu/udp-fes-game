package packets;


public class Packet02Move extends Packet {

    private String username;
    private float x, y, direction;

    public Packet02Move(byte[] data) {
        super(02);
        String[] dataArray = readData(data).split(",");
        this.username = dataArray[0];
        this.x = Float.parseFloat(dataArray[1]);
        this.y = Float.parseFloat(dataArray[2]);
        this.direction = Float.parseFloat(dataArray[3]);

    }

    public Packet02Move(String username, float x, float y, float direction) {
        super(02);
        this.username = username;
        this.x = x;
        this.y = y;
        this.direction = direction;
    }


    @Override
    public byte[] getData() {
        return ("02" + this.username + "," + this.x + "," + this.y+","+this.direction).getBytes();

    }

    public String getUsername() {
        return username;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }
    public float getDirection() {
        return this.direction;
    }
}
