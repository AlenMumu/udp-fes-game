package packets;

/**
 * Created by amujkic on 26.05.2014.
 */
public class Packet11StartStopGame extends Packet {


    private String startStop;



    public Packet11StartStopGame(byte[] data) {
        super(11);
        String[] dataArray = readData(data).split(",");

        this.startStop = dataArray[0];
    }

    public Packet11StartStopGame(String mapName) {
        super(11);

        this.startStop = mapName;
    }

    @Override
    public byte[] getData() {
        return  new String("11"+this.startStop).getBytes();
    }



    public String getStartStop() {
        return startStop;
    }

    public void setStartStop(String startStop) {
        this.startStop = startStop;
    }
}
