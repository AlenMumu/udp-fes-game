package packets;

/**
 * Created by amujkic on 15.05.2014.
 */
public class Packet06Room extends Packet {


private int maxPlayers;
    private int currentPlayers;
    private String roomName;
    private String mapName;



    public Packet06Room(byte[] data) {
        super(06);
        String[] dataArray = readData(data).split(",");
        this.maxPlayers = Integer.valueOf(dataArray[0]);
        this.currentPlayers = Integer.valueOf(dataArray[1]);
this.roomName = dataArray[2];
        this.mapName = dataArray[3];
    }

    public Packet06Room(int maxPlayers,int currentPlayers,String roomName,String mapName) {
        super(06);

        this.maxPlayers = maxPlayers;
        this.currentPlayers = currentPlayers;

        this.roomName = roomName;
        this.mapName = mapName;
    }

    @Override
    public byte[] getData() {
        return  new String("06"+this.maxPlayers+","+this.currentPlayers+","+this.roomName+","+this.mapName).getBytes();
    }


    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public int getCurrentPlayers() {
        return currentPlayers;
    }

    public void setCurrentPlayers(int currentPlayers) {
        this.currentPlayers = currentPlayers;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }
}
