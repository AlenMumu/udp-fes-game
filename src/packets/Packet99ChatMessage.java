package packets;

/**
 * Created by amujkic on 25.04.2014.
 */
public class Packet99ChatMessage extends Packet {

    private String username;
    private String message;
    private String date;


    public Packet99ChatMessage(byte[] data) {
        super(99);
        String[] dataArray = readData(data).split(",");
        this.date = dataArray[0];
        this.username = dataArray[1];
        this.message = dataArray[2];
    }

    public Packet99ChatMessage(String date,String username,String message) {
        super(99);
        this.date = date;
        this.username = username;
        this.message = message;
    }


    @Override
    public byte[] getData() {
        return ("99" + this.date + "," + this.username+ "," + this.message).getBytes();

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
