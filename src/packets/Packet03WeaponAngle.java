package packets;

/**
 * Created by alen on 4/23/14.
 */
public class Packet03WeaponAngle extends Packet {

    private String username;
    private float x,y, angle;

    public Packet03WeaponAngle(byte[] data) {
        super(03);
        String[] dataArray = readData(data).split(",");
        this.username = dataArray[0];

        this.x = Float.parseFloat(dataArray[1]);
        this.y = Float.parseFloat(dataArray[2]);
this.angle = Float.parseFloat(dataArray[3]);
    }

    public Packet03WeaponAngle(String username, float x, float y, float angle) {
        super(03);
        this.username = username;
this.x = x;
        this.y=y;
        this.angle = angle;
    }


    @Override
    public byte[] getData() {
        return ("03" + this.username + "," +this.x+","+this.y+","+this.angle).getBytes();

    }

    public String getUsername() {
        return username;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }
}